#!/bin/bash

while true; do
    read -p "What is the url of you mobilizon instance? (https://domain.tld) " url
    case $url in
        "https://"*"."*[^/] ) break;;
        * ) echo "Please give the complete uri https://domain.tld";;
    esac
done

SCRIPT_DIR=$(dirname -- ${BASH_SOURCE[0]})
if [ ! -f "$SCRIPT_DIR/mobilizon-poster/config.json" ]; then
    echo -e "{\n  \"api\":\"$url/api\"\n}" > "$SCRIPT_DIR/mobilizon-poster/config.json"
fi

if [[ $(command -v google-chrome) && $(command -v chromium) ]]; then
    echo "Both, google-chrome (1) and chromium (2) are installed. Which one do you want to use? "
    select chrome in "google-chrome" "chromium"
    do
        echo "chrome=$(command -v $chrome)" > "$SCRIPT_DIR/.env"; break;
    done
fi

echo "credentials to login to you mobilizon account:"
read -p "email: " login
prompt="password:"
while IFS= read -p "$prompt" -r -s -n 1 char
do
    if [[ $char == $'\0' ]]
    then
        break
    fi
    prompt='*'
    password+="$char"
done
echo
echo -e "login=\"$login\"\npassword=\"$password\"" > "$SCRIPT_DIR/credentials"
echo "created credential file $SCRIPT_DIR/credentials."

