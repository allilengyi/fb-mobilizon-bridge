# fb-mobilizon-bridge

The fb-mobilizon-bridge consists of 3 parts:

1. The librevent project that extracts event details from facebook event pages
2. The mobilizon-poster that publishes events  on a configured mobilizon instance
3.  The bridge itself that glues the other projects together

The librevent project is generally a browser extension. But we use only the part that opens (via puppetteer) facebook event pages in a chrome based browser to fetch the event details.

## Install

Requirements: 

* nodejs / npm
* jq 

To install jq, npm (if necessary) and nodejs modules open a terminal and run 

```
git clone https://framagit.org/allilengyi/fb-mobilizon-bridge
cd fb-mobilizon-bridge
bash install-bridge.sh
```

If jq or npm needs to be installed, the aptitude package manager would be used by the script.

## Setup

To store information on the mobilizon instance used to publish the event and credentials of your mobilizon account in `.\credentials`call

```
bash setup.sh
```

## Getting started

There are a few assumptions made:

* Mobilizon groups publish the events

* Events facebook page will not be splitted across several mobilizon groups
* There is a fixed address and a fixed url of the mobilizon event (we have in mind the address and url of a club)
* We use the schema of facebooks event pages `https://facebook.com/<location>/events/`

To liberate events from facebook to a mobilizon instance run

```
bash bridge.sh <location>
```

On first usage you need to configure the chrom[e|ium] browser that is used. Visit facebook.com, manage cookies, ensure English language. Hit enter in the terminal.

Give it a try. However, this bridge is not well tested. Feedback on the installation and setup process is welcome. 

