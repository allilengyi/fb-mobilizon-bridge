#!/bin/bash

# define absolute paths to
SCRIPT_DIR=$(realpath $(dirname -- "${BASH_SOURCE[0]}"))
libreventPATH="$SCRIPT_DIR/librevent/automation"
posterPATH="$SCRIPT_DIR/mobilizon-poster"
fb2mobPATH=$SCRIPT_DIR

source $fb2mobPATH/credentials
source $fb2mobPATH/.env
source $fb2mobPATH/transform_time.sh

venue=$1

if [ ! -f $fb2mobPATH/credentials ]; then
  echo "the credential file $fb2mobPATH/credentials does not exist!";
  exit 2;
fi

if [ ! -f $fb2mobPATH/$venue.json ]; then
  echo "the json file $fb2mobPATH/$venue.json belonging to the fb page does not exist.";
  read -p "location of the venue, e.g. wild at heart berlin: " location
  read -p "url of the venue website (can be empty): " url
  echo "List available groups"
  cd $posterPATH
  node bin/login.js --login=$login --password=$password &>/dev/null
  if [ $(jq '.identities' identities.json | jq length) -eq 1 ]; then
    actor=$(jq '.identities[].id' identities.json)
  else
    jq '.identities[] | {id,name}' identities.json
    read -p "choose actor to publish events on behalf of a group to be selected. id: " actor
  fi
  node bin/group-list.js
    jq --arg actor_id $actor '.[] | select(.role | IN ("ADMINISTRATOR", "MODERATOR")) | select(.actor.id==$actor_id) | .parent | {id,name}' groups.json
  read -p "group Id of the group who publish the events of the venue. id: " id
  echo -e "{\n  \"address\":\"$location\",\n  \"url\":\"$url\",\n  \"attributedToId\":\"$id\",\n  \"organizer_id\":\"$actor\",\n  \"events\": []\n}" > "$fb2mobPATH/$venue.json"
fi

cd $libreventPATH
# now=`date --utc +%Y-%m-%dT%H:%M:%SZ`
# evIds=( "$(jq -r --arg now $now '.events[] | select(.start > $now) | .id' $fb2mobPATH/$venue.json)" )
# printf -v joined '%s,' "${evIds[@]}"
# ## delete event details and screencaptures
# rm evdetails/*.json &>/dev/null
# rm screencapts/* &>/dev/null

# node bin/libertadata.js --page https://www.facebook.com/$venue/events/ --skip "${joined%,}" --chrome $chrome

events=( "$(ls $libreventPATH/evdetails/ | grep -oE "^[0-9]+" | sort | uniq)" )

if [ "${#events[@]}" -eq "0" ]; then
	exit 0;
fi

for event in ${events[@]}
do
  ev_id=$(echo $event | sed 's/[^0-9]//g')
  file=$(ls $libreventPATH/evdetails/${event}*.json)
  when=$(jq -r '.when' $file)
  jq --arg startTime "$(get_startTime "$when")" --arg endTime "$(get_endTime "$when")" '.mobilizonDates= { "start": $startTime, "end": $endTime }' $file > $libreventPATH/evdetails/mob${event}.json
done
	
jq '.when, .mobilizonDates' $libreventPATH/evdetails/mob*.json

echo  "Are the dates correct?"
sleep 3

cd $posterPATH
node bin/login.js --login=$login --password=$password &>/dev/null

for event in ${events[@]}
do
# check if event has already been crossposted
  if [ -z "$(jq --arg ev_id $event '.events[] | select(.id == $ev_id)' $fb2mobPATH/$venue.json)" ]; then
   file=$(ls $libreventPATH/evdetails/${event}*.json)
   when=$(jq -r '.when' $file)
   description=$(jq '.description' $file)
   if [[ "$description" == "null" ]]; then 
	description="<p>no description available</p>"
   else
	description=$(echo $description | sed 's/\\n\\n/<\/p><p>/g' | sed 's/\\n/<br>/g' |  sed 's/.\{1\}/<p>/' | sed 's/.$/<\/p>/') 
   fi
   title=$(jq '.title' $file)
   location=$(jq '.location' $file)
   echo $location >> $fb2mobPATH/$venue.locations
   image=$(jq -r '.saved' $file)

   start=$(jq -r '.mobilizonDates.start' $libreventPATH/evdetails/mob$event.json ) 
   end=$(jq -r '.mobilizonDates.end' $libreventPATH/evdetails/mob$event.json )

   address=$(jq '.address' $fb2mobPATH/$venue.json)
   url=$(jq '.url' $fb2mobPATH/$venue.json)
   if [[ ! "$end" == "null" ]]; then end=\"$end\"; fi
   cd $posterPATH
   output=$(./uploader.sh "$event" "$libreventPATH/${image}")
   media_id=null
   if  [ "$output" ]; then
        media_id=$(jq '.data.uploadMedia.id' <<<$output)
   fi
   # missing: attributed_to_id and organizer_id
   # jq '.[3].preferredusername' groups.json <- in this case output is aboutblank
   # jq length groups.json <- use this to loop through entries and search for groupname
   #attributed_to_id=$(jq '.[3].parent.id' groups.json) 
   attributed_to_id=$(jq '.attributedToId' $fb2mobPATH/$venue.json)
   organizer_id=$(jq '.organizer_id' $fb2mobPATH/$venue.json)
     
   cd $fb2mobPATH
   jq <<<"{\"start\": \"$start\",\"end\": $end,\"title\": $title,\"description\": \"$description\", \"address\": $address, \"url\": $url, \"picture\": {\"media_id\": $media_id}, \"organizer_id\":$organizer_id, \"attributed_to_id\": $attributed_to_id}" > eventvars.json
   # call mobilizon poster
   cd $posterPATH
   post_result=$(node bin/postEvent.js $fb2mobPATH/eventvars.json)
   mobilizon_event_id=$(tmpresult=${post_result:(-25)}; echo "${tmpresult//[!0-9]/}")
   startUtc=`date -u -d @$(date -d"$start" +%s) +%Y-%m-%dT%H:%M:%SZ`
   if [ -z "$startUtc" ]; then
    continue;
   fi
   jq --arg startUtc $startUtc --arg fbId $event --arg mobId $mobilizon_event_id '.events[.events| length] |= .+ {"fbId":$fbId, "start":$startUtc, "mobId":$mobId}' $fb2mobPATH/$venue.json > $fb2mobPATH/$venue.json.tmp
   mv $fb2mobPATH/$venue.json.tmp $fb2mobPATH/$venue.json
  # end of crosspost check
  fi
done
