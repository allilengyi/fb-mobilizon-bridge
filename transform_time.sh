#!/bin/bash
function get_startTime () {
	when="$1"
	if [ "$(echo "$when" | awk -F, '{ print $2 $3}' | sed 's/PM.*/PM/' | sed 's/AM.*/AM/' | sed 's/at //')" ]; then
		date -d "$(echo "$when" | awk -F, '{ print $2 $3}' | sed 's/PM.*/PM/' | sed 's/AM.*/AM/' | sed 's/at //')" +%Y-%m-%d\ %H:%M
	elif [ "$(echo "$when" | sed 's/PM.*/PM/' | sed 's/AM.*/AM/' | sed 's/at //')" ]; then
		date -d "$(echo "$when" | sed 's/PM.*/PM/' | sed 's/AM.*/AM/' | sed 's/at //')" +%Y-%m-%d\ %H:%M
	else
		echo null
	fi
}

function get_endTime () {
	when="$1"
	echo "$when" | grep -q "–";
	if [ $? != 0 ]; then
		echo null
		exit 1;
	fi

	if [ "$(echo "$when" | awk -F, '{ print $2 $3}' | sed 's/ at.*–//')" ]; then
		endTime="$(date -d "$(echo "$when" | awk -F, '{ print $2 $3}' | sed 's/ at.*–//')" +%Y-%m-%d\ %H:%M)"
		if [[ "$(get_startTime "$when")" > "$endTime" ]]; then
			date -d "$(date -d "$(date -d "$endTime" +%Y-%m-%d) + 1 day" +%Y-%m-%d) $(date -d "$endTime" +%H:%M)" +%Y-%m-%d\ %H:%M
		else
			echo "$endTime"
		fi
	elif [ "$(echo "$when" | sed 's/.*–//' | sed 's/at //')" ]; then
		endTime="$(date -d "$(echo "$when" | sed 's/.*–//' | sed 's/at //')" +%Y-%m-%d\ %H:%M)"
		if [[ "$(get_startTime "$when")" > "$endTime" ]]; then
			date -d "$(date -d "$(date -d "$endTime" +%Y-%m-%d) + 1 day" +%Y-%m-%d) $(date -d "$endTime" +%H:%M)" +%Y-%m-%d\ %H:%M
		else
			echo "$endTime"
		fi
	fi
}
