#!/bin/bash

# check dependencies
if [ -z $(command -v npm) ]; then
  zenity --title "Dependency is missing" --text "npm is not installed. Do you want to install it? (sudo privilege required)" --question --width=300
  if [ $? -eq 1 ]; then echo "Rejected to install dependency. Aborting"; exit; fi
  sudo apt install npm  
fi
if [ -z $(command -v jq) ]; then
  zenity --title "Dependency is missing" --text "jq is not installed. Do you want to install it? (sudo privilege required)" --question --width=300
  if [ $? -eq 1 ]; then echo "Rejected to install dependency. Aborting"; exit; fi
  sudo apt install jq  
fi
if [[ -z $(command -v chromium) && -z $(command -v google-chrome) ]]; then
  zenity --title "Chrome browser is missing" --info --text "Please install ungoogled-chromium, chromium or google-chrome" --width=300
fi

echo "installing node modules..."
SCRIPT_DIR=$( dirname -- "${BASH_SOURCE[0]}" )
cd "$SCRIPT_DIR/librevent/extension"
npm install
npm run build:dist
cd ../automation
npm install
cd ../..

cd "$SCRIPT_DIR/mobilizon-poster"
npm install
cd ..

while true; do
    read -p "Do you wish to continue with the setup of the bridge? [y|n]" yn
    case $yn in
        [Yy]* ) bash "$SCRIPT_DIR/setup.sh"; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

